//#pragma comment(lib, "opencv_world454.lib")
#include "erodeplugin.h"


QString ErodePlugin::name()
{
    return "Erode";
}

void ErodePlugin::edit(const cv::Mat &input, cv::Mat &output)
{
    erode(input, output, cv::Mat()); //调用opencv的erode()操作（腐蚀）
    ///侵蚀是缩小图像前景或 1 值对象的过程。
    ///它可以平滑对象边界并去除半岛，手指和小物体

}
