
#include "sharpenplugin.h"

QString SharpenPlugin::name()
{
    return "Sharpen";
}

void SharpenPlugin::edit(const cv::Mat &input, cv::Mat &output)
{
    int intensity = 2;
    cv::Mat smoothed; //图像平滑版本
    //第4个变量表示 X 方向上的高斯核标准差
    cv::GaussianBlur(input, smoothed, cv::Size(9, 9), 0); //平滑处理
    output = input + (input - smoothed) * intensity; // * intensity是在叠加
}

