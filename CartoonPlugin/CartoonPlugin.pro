QT += gui

TEMPLATE = lib
CONFIG += plugin
TARGET = CartoonPlugin
# . 表示 .pro文件的路径
INCLUDEPATH += ..\ImageViewer \ # editor_plugin_interface.h
# opencv
                F:\OpenCV\opencv\build\include \
                F:\OpenCV\opencv\build\include\opencv2

LIBS += F:\OpenCV\opencv\build\x64\vc15\lib\opencv_world454.lib \
        F:\OpenCV\opencv\build\x64\vc15\lib\opencv_world454d.lib \


CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cartoonplugin.cpp

HEADERS += \
    cartoonplugin.h

DISTFILES += CartoonPlugin.json

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
