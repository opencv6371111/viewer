
#include "cartoonplugin.h"

QString CartoonPlugin::name()
{
    return "Cartoon";
}

void CartoonPlugin::edit(const cv::Mat &input, cv::Mat &output)
{
    int num_down = 2; //缩小的次数
    int num_bilateral = 7; //小的双边过滤器次数

    cv::Mat copy1, copy2;
    cv::Mat image_gray, image_edge;

    copy1 = input.clone();
    for(int i = 0; i < num_down; i++) {
        cv::pyrDown(copy1, copy2); //cv::pyrDown重复缩小copy1的大小，结果保存在copy2
        copy1 = copy2.clone();
    }

    //缩小调色板
    for(int i = 0; i < num_bilateral; i++) {
        //第3~5参数是：像素邻域的直径、色彩空间过滤器、坐标中的过滤器
        cv::bilateralFilter(copy1, copy2, 9, 9, 7); //对copy1应用一个小的双边过滤器
        copy1 = copy2.clone();
    }

    //向下采样将图像放大到原始大小
    for(int i = 0; i < num_down; i++) {
        cv::pyrUp(copy1, copy2); //调用cv::pyrUp的次数与在其上调用cv::pyrDown相同
        copy1 = copy2.clone();
    }

    //因为数学上的原因，可能等于或大于原始像素几个像素，
    //所以应将copy1调整为原始图片的尺寸。
    if (input.cols != copy1.cols  || input.rows != copy1.rows) {
        cv::Rect rect(0, 0, input.cols, input.rows);
        copy1(rect).copyTo(copy2);
        copy1 = copy2;
    }

    cv::cvtColor(input, image_gray, cv::COLOR_RGB2GRAY);
    //应用中值过滤器，过滤噪点
    cv::medianBlur(image_gray, image_gray, 5);
    //边缘检测，以THRESH_BINARY作为其阈值，该阈值由当前像素周围较小区域中的像素确定，
    //这样，可以检测每个小区域中最显着的特征。
    //得到在图像中的对象周围绘制粗体和黑色轮廓的地方。
    cv::adaptiveThreshold(image_gray, image_gray, 255,
                          cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 9, 2);

    cv::cvtColor(image_gray, image_edge, cv::COLOR_GRAY2RGB);

    output = copy1 & image_edge; //按位and操作合并

    /*
    cv::GaussianBlur(image_edge, image_edge, cv::Size(5, 5), 0);
    cv::Mat mask(input.rows, input.cols, CV_8UC3, cv::Scalar(90, 90, 90));
    mask = mask & (~image_edge);
    output = (copy1 & image_edge) | mask;
    */
}

