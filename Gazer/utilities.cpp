#include <QObject>
#include <QApplication>
#include <QDateTime>
#include <QDir>
#include <QStandardPaths>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QHostInfo>
#include <QDebug>

#include "utilities.h"

QString Utilities::getDataPath()
{
    //QStandardPaths::standardLocations(QStandardPaths::MoviesLocation)静态方法
    //并拾取其中的第一个元素来保存视频
//    QString user_movie_path = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation)[0];
    //使用F盘
    QString user_movie_path = "F:/OpenCV/";
    QDir movie_dir(user_movie_path);
    movie_dir.mkpath("Gazer"); //创建Gazer子目录
    return movie_dir.absoluteFilePath("Gazer");
}

QString Utilities::newSavedVideoName()
{
    //使用日期和时间来生成新名称
    QDateTime time = QDateTime::currentDateTime();
    return time.toString("yyyy-MM-dd+HHmmss");
}

QString Utilities::getSavedVideoPath(QString name, QString postfix)
{
    //将给定路径+名称+.后缀，返回文件绝对路径
    return QString("%1/%2.%3").arg(Utilities::getDataPath(), name, postfix);
}

void Utilities::notifyMobile(int cameraID)
{
    QString endpoint = "https://maker.ifttt.com/trigger/...";
    // CHANGE endpoint TO YOURS HERE:
    // https://maker.ifttt.com/trigger/Motion-Detected-by-Gazer/with/key/-YOUR_KEY
    // QString endpoint = QUrl("https://maker.ifttt.com/YOUR_END PIOIN");
    QNetworkRequest request = QNetworkRequest(QUrl(endpoint));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject json;
    json.insert("value1", QString("%1").arg(cameraID));
    json.insert("value2", QHostInfo::localHostName());
    QNetworkAccessManager nam;
    QNetworkReply *rep = nam.post(request, QJsonDocument(json).toJson());
    while(!rep->isFinished()) {
        QApplication::processEvents();
    }
    // QString strReply = (QString)rep->readAll();
    // qDebug()<<"Test: "<<strReply;
    rep->deleteLater();
}

