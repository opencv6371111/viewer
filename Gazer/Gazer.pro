QT       += core5compat core gui multimedia network concurrent
#QT       += multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += F:\OpenCV\opencv\build\include \
               F:\OpenCV\opencv\build\include\opencv2

LIBS += F:\OpenCV\opencv\build\x64\vc15\lib\opencv_world454d.lib

# 使用Qt的捕获线程、而不是OpenCV的
#DEFINES += GAZER_USE_QT_CAMERA=1

SOURCES += \
    capture_thread.cpp \
    main.cpp \
    mainwindow.cpp \
    utilities.cpp

HEADERS += \
    capture_thread.h \
    mainwindow.h \
    utilities.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
