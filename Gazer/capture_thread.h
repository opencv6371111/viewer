
#ifndef CAPTURE_THREAD_H
#define CAPTURE_THREAD_H

#include <QString>
#include <QThread>
#include <QMutex>

#include "opencv2/opencv.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/video/background_segm.hpp"

using namespace std;

class CaptureThread : public QThread
{
    Q_OBJECT
public:
    //第1个参数：摄像头索引
    CaptureThread(int camera, QMutex *lock);
    //第1个参数：视频文件路径。用视频文件模拟摄像头
    CaptureThread(QString videoPath, QMutex *lock);
    ~CaptureThread();
    //捕获线程的运行状态
    void setRunning(bool run) {running = run; }
    void startCalcFPS() {fps_calculating = true; }

    enum VideoSavingStatus //捕获线程中视频保存工作的状态
    {
        STARTING,
        STARTED,
        STOPPING,
        STOPPED
    };

    void setVideoSavingStatus(VideoSavingStatus status) {video_saving_status = status; }
    void setMotionDetectingStatus(bool status) {
        motion_detecting_status = status;
        motion_detected = false;
        if(video_saving_status != STOPPED) video_saving_status = STOPPING;
    }

protected:
    void run() override; //线程的起点，线程.start()后会自动调用其中的run()

signals:
    void frameCaptured(cv::Mat *data); //每次摄像头捕获帧时，发出此信号
    void fpsChanged(float fps); //完成fps计算后发射
    void videoSaved(QString name); //完成视频保存所有工作后发射

private:
    void calculateFPS(cv::VideoCapture &cap);
    void startSavingVideo(cv::Mat &firstFrame); //保存首帧为封面
    void stopSavingVideo(); //清理工作
    void motionDetect(cv::Mat &frame);

private:
    bool running;
    int cameraID;
    QString videoPath;
    QMutex *data_lock; //数据保护锁
    cv::Mat frame; //当前捕获帧

    // FPS calculating
    bool fps_calculating; //捕获线程是否正在执行FPS计算
    float fps; //保存计算的fps

    // video saving
    int frame_width, frame_height;
    VideoSavingStatus video_saving_status;
    QString saved_video_name;
    cv::VideoWriter *video_writer; //视频写入器，写入捕获的帧

    // motion analysis
    bool motion_detecting_status;
    bool motion_detected;
    cv::Ptr<cv::BackgroundSubtractorMOG2> segmentor;
};

#endif // CAPTURE_THREAD_H
