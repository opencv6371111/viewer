
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QAction>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QStatusBar>
#include <QLabel>
#include <QListView>
#include <QCheckBox>
#include <QPushButton>
#include <QGraphicsPixmapItem>
#include <QMutex>
#include <QStandardItemModel>

//使用Qt捕获视频，不使用OpenCV捕获视频
#ifdef GAZER_USE_QT_CAMERA
#include <QMediaCaptureSession>
#include <QCamera>
#endif

#include "opencv2/opencv.hpp"
#include "capture_thread.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow

{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initUI();
    void createActions(); //菜单栏创建操作
    void populateSavedList(); //打开应用，读取已保存的视频文件

private slots:
    void showCameraInfo();
    void openCamera(); //创建新的捕获线程
    void updateFrame(cv::Mat*); //
    void calculateFPS();
    void updateFPS(float);
    void recordingStartStop(); //改变视频保存操作状态
    void appendSavedVideo(QString name); //用视频名称调用该slot
//    void updateMonitorStatus(int status);

private:
    Ui::MainWindow *ui;

    QMenu *fileMenu;

    QAction *cameraInfoAction;
    QAction *openCameraAction;
    QAction *calcFPSAction;
    QAction *exitAction;

    QGraphicsScene *imageScene; //管理图像
    QGraphicsView *imageView; //显示图像



#ifdef GAZER_USE_QT_CAMERA
    QCamera *camera;
    QMediaCaptureSession *videoSession;
#endif

    QCheckBox *monitorCheckBox; //监视器的状态：是否开始或停止录制视频
    QPushButton *recordButton; //保存视频按钮

    //模型/视图
    QListView *saved_list; //已保存视频的列表
    QStandardItemModel *list_model; //为QListView对象saved_list提供数据

    QStatusBar *mainStatusBar; //状态栏
    QLabel *mainStatusLabel;

    cv::Mat currentFrame; //存储捕获线程捕获的帧

    // for capture thread
    QMutex *data_lock; //保护CaptureThread.frame的数据

    CaptureThread *capturer; //捕获线程的句柄（用户打开摄像头时，用它进行视频捕获）
};

#endif // MAINWINDOW_H
