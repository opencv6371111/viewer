
#ifndef UTILITIES_H
#define UTILITIES_H
#include <QString>

//助手类
//给视频保存文件命名
class Utilities
{
public:
    //返回我们将在其中保存视频文件的目录
    static QString getDataPath();
    //保存的视频生成一个新名称
    static QString newSavedVideoName();
    //接受名称和后缀（扩展名），并返回具有给定名称的视频文件的绝对路径
    static QString getSavedVideoPath(QString name, QString postfix);
    static void notifyMobile(int cameraID);
};

#endif // UTILITIES_H
