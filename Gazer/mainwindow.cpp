#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QKeyEvent>
#include <QDebug>
#include <QCameraDevice>
#include <QMediaDevices>
#include <QGridLayout>
#include <QIcon>
#include <QStandardItem>
#include <QSize>
//#include <QVideoWidget>

#include "opencv2/videoio.hpp"

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utilities.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , fileMenu(nullptr)
    , capturer(nullptr)
{
    ui->setupUi(this);
    initUI();
    data_lock = new QMutex();
}

void MainWindow::initUI()
{
    this->resize(1000, 700);

    //设置菜单栏
    fileMenu = menuBar()->addMenu("&File");

    //////////设置主窗口的中心区域
    QGridLayout *main_layout = new QGridLayout();

#ifdef GAZER_USE_QT_CAMERA
    QList<QCameraDevice> cameras = QMediaDevices::videoInputs();
    // I have two cemaras and use the first one here
    camera = new QCamera(cameras[0]);
    videoSession = new QMediaCaptureSession;
    videoSession->setCamera(camera);

    // 创建视频显示控件
    QVideoWidget *videoWidget = new QVideoWidget;
    videoWidget->resize(800, 600);
    // 将视频显示控件设置为视频会话的输出目标
    videoSession->setVideoOutput(videoWidget);
    main_layout->addWidget(videoWidget, 0, 0, 12, 1);

#else
    imageScene = new QGraphicsScene(this);
    imageView = new QGraphicsView(imageScene);

    //矩形小部件：（添加到imageView，开始行，开始列，它跨越的行数，跨越的列数）
    main_layout->addWidget(imageView, 0, 0, 12, 1); //向布局中添加小部件
#endif

    /////////在主窗口中嵌套另一个网络布局
    QGridLayout *tools_layout = new QGridLayout();
    //后面四个参数是位置矩形
    main_layout->addLayout(tools_layout, 12, 0, 1, 1); //从第13行开始

    monitorCheckBox = new QCheckBox(this);
    monitorCheckBox->setText("Monitor On/Off");
    //因为tools_layout布局中只用一行、一列，所以只需要提供行、列索引定位单元格
    tools_layout->addWidget(monitorCheckBox, 0, 0);

    recordButton = new QPushButton(this);
    recordButton->setText("Record");
    //AlignHCenter定位该单元格的中央
    tools_layout->addWidget(recordButton, 0, 1, Qt::AlignHCenter);

    tools_layout->addWidget(new QLabel(this), 0, 2); //占位
    connect(recordButton, SIGNAL(clicked(bool)), this, SLOT(recordingStartStop()));


    ////////////窗口下方；已保存的视频
    saved_list = new QListView(this);
    saved_list->setViewMode(QListView::IconMode); //使用大尺寸的LeftToRight流来布局
    saved_list->setResizeMode(QListView::Adjust); //每次调整视图大小时都会布局其项目
    saved_list->setSpacing(5); //间距
    saved_list->setWrapping(false); //无论有多少项目，所有项目都将放置在一行中
    list_model = new QStandardItemModel(this);
    saved_list->setModel(list_model);
    main_layout->addWidget(saved_list, 13, 0, 4, 1); //从第14行开始

    ////////////显示
    /// 本小部件以主窗口布局作为其布局，设置为主窗口中央
    QWidget *widget = new QWidget();
    widget->setLayout(main_layout);
    setCentralWidget(widget);

    ////////////设置状态栏和操作
    mainStatusBar = statusBar(); //状态栏
    mainStatusLabel = new QLabel(mainStatusBar);
    mainStatusBar->addPermanentWidget(mainStatusLabel);
    mainStatusLabel->setText("Gazer is Ready");

    createActions();
    populateSavedList();
}

//创建QActions实例，将其添加到“File”菜单中
void MainWindow::createActions()
{
    // create actions, add them to menus
    cameraInfoAction = new QAction("Camera &Information", this);
    fileMenu->addAction(cameraInfoAction);
    openCameraAction = new QAction("&Open Camera", this);
    fileMenu->addAction(openCameraAction);
    calcFPSAction = new QAction("&Calculate FPS", this);
    fileMenu->addAction(calcFPSAction);
    exitAction = new QAction("E&xit", this);
    fileMenu->addAction(exitAction);

    // connect the signals and slots
    connect(exitAction, SIGNAL(triggered(bool)), QApplication::instance(), SLOT(quit()));
    connect(cameraInfoAction, SIGNAL(triggered(bool)), this, SLOT(showCameraInfo()));
    connect(openCameraAction, SIGNAL(triggered(bool)), this, SLOT(openCamera()));
    connect(calcFPSAction, SIGNAL(triggered(bool)), this, SLOT(calculateFPS()));
}

void MainWindow::populateSavedList()
{
    QDir dir(Utilities::getDataPath());
    QStringList nameFilters;
    nameFilters << "*.jpg";
    QFileInfoList files = dir.entryInfoList(
        nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);

    foreach(QFileInfo cover, files) {
        QString name = cover.baseName();
        QStandardItem *item = new QStandardItem();
        list_model->appendRow(item);
        QModelIndex index = list_model->indexFromItem(item);
        list_model->setData(index, QPixmap(cover.absoluteFilePath()).scaledToHeight(145), Qt::DecorationRole);
        list_model->setData(index, name, Qt::DisplayRole);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::showCameraInfo()
{
    QList<QCameraDevice> cameras = QMediaDevices::videoInputs();
    QString info = QString("Available Cameras: \n");

    foreach (const QCameraDevice &cameraInfo, cameras) {
        info += cameraInfo.description() + "\n";
    }
    QMessageBox::information(this, "Cameras", info);
}


#ifdef GAZER_USE_QT_CAMERA
void MainWindow::openCamera()
{
    qDebug()<<"now is using Qt Camera Capture\n";
    //QCamera类将自动处理线程，所以无需使用自定的线程类
    camera->start();
}
#else
void MainWindow::openCamera()
{
    //用户多次单击“打开摄像机”操作，则会创建多个捕获线程，并且它们将同时运行。
    //因此，在启动新线程之前，必须检查是否已经在运行一个线程，如果存在，则应该在启动新线程之前将其停止。
    if(capturer != nullptr)
    {
        mainStatusLabel->setText(QString("Capturing Camera has alreay start!"));
        return;
//        //停下线程，并断开连接的信号和插槽（试了效果，主窗口视频会卡住）
//        capturer->setRunning(false);
//        disconnect(capturer, &CaptureThread::frameCaptured, this, &MainWindow::updateFrame);
////        disconnect(capturer, &CaptureThread::fpsChanged, this, &MainWindow::updateFPS);
////        disconnect(capturer, &CaptureThread::videoSaved, this, &MainWindow::appendSavedVideo);

//        //将其自身的新插槽deleteLater连接到其finished信号.
//        //在run方法返回时，线程将进入其生命周期的尽头，并且将发出其finished信号。
//        //Qt 库将删除该线程实例。
//        connect(capturer, &CaptureThread::finished, capturer, &CaptureThread::deleteLater);
    }
//    // I am using my second camera whose Index is 2.  Usually, the
//    // Index of the first camera is 0.
    int camID = 0;
    capturer = new CaptureThread(camID, data_lock);
    connect(capturer, &CaptureThread::frameCaptured, this, &MainWindow::updateFrame);
    connect(capturer, &CaptureThread::fpsChanged, this, &MainWindow::updateFPS);
    connect(capturer, &CaptureThread::videoSaved, this, &MainWindow::appendSavedVideo);
    capturer->start(); //启动捕获线程
    mainStatusLabel->setText(QString("Capturing Camera %1").arg(camID));
//    monitorCheckBox->setCheckState(Qt::Unchecked);
//    recordButton->setText("Record");
//    recordButton->setEnabled(true);
}
#endif


//一帧帧捕获后，显示在主窗口主区域
void MainWindow::updateFrame(cv::Mat *mat)
{
    data_lock->lock();
    currentFrame = *mat;
    data_lock->unlock();
    QImage frame(
        currentFrame.data,
        currentFrame.cols,
        currentFrame.rows,
        currentFrame.step,
        QImage::Format_RGB888);
    QPixmap image = QPixmap::fromImage(frame);
    imageScene->clear();
    imageView->resetTransform();
    imageScene->addPixmap(image);
    imageScene->update();
    imageView->setSceneRect(image.rect());
}

void MainWindow::calculateFPS()
{
    if(capturer != nullptr)
    {
        capturer->startCalcFPS();
    }
}

void MainWindow::updateFPS(float fps)
{
    mainStatusLabel->setText(QString("FPS of current camera is %1").arg(fps));
}


void MainWindow::recordingStartStop() {
    QString text = recordButton->text();
    if(text == "Record" && capturer != nullptr)
    {
        capturer->setVideoSavingStatus(CaptureThread::STARTING);
        recordButton->setText("Stop Recording");
        monitorCheckBox->setCheckState(Qt::Unchecked);
        monitorCheckBox->setEnabled(false);
    }
    else if(text == "Stop Recording" && capturer != nullptr)
    {
        capturer->setVideoSavingStatus(CaptureThread::STOPPING);
        recordButton->setText("Record");
        monitorCheckBox->setEnabled(true);
    }
}

void MainWindow::appendSavedVideo(QString name)
{
    QString cover = Utilities::getSavedVideoPath(name, "jpg");
    //将一个空项目用作占位符，在其位置将大图像设置为装饰数据(QStandardItem是带有标准图标图像和字符串的项目)
    QStandardItem *item = new QStandardItem();
    list_model->appendRow(item); //附加到视图模型list_model中
    QModelIndex index = list_model->indexFromItem(item);
    //位置由找到的Qt::DecorationRole角色索引所指定
    list_model->setData(index, QPixmap(cover).scaledToHeight(145), Qt::DecorationRole);
    //视频名称设置为Qt::DisplayRole角色在相同位置的显示数据
    list_model->setData(index, name, Qt::DisplayRole);
    saved_list->scrollTo(index); //列表视图滚动到新添加项目的索引
}

