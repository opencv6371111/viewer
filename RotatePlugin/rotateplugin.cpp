
#include "rotateplugin.h"

QString RotatePlugin::name()
{
    return "Rotate";
}

//仿射变换
void RotatePlugin::edit(const cv::Mat &input, cv::Mat &output)
{
    double angle = 45.0;
    double scale = 1.0;
    cv::Point2f center = cv::Point(input.cols/2, input.rows/2);

    //生成旋转矩阵，需指定中心点、角度(逆时针)、缩放比例
    cv::Mat rotateMatrix = cv::getRotationMatrix2D(center, angle, scale);

    //插值方法：INTER_LINEAR
    //输出图像边界的像素外推方法：BORDER_CONSTANT（旋转后，如果某些区域未被原始图像覆盖，
    //则将用恒定的颜色填充，默认黑色）。但是旋转会原始位置会有边界周围像素丢失。
    cv::Mat result;
    cv::warpAffine(input, result,
                   rotateMatrix, input.size(),
                   cv::INTER_LINEAR, cv::BORDER_CONSTANT);
    output = result;
}
