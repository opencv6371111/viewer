
#ifndef ROTATEPLUGIN_H
#define ROTATEPLUGIN_H

#include <QObject>
#include <QtPlugin>

#include "editor_plugin_interface.h"

class RotatePlugin :public QObject, public EditorPluginInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID EDIT_PLUGIN_INTERFACE_IID); //声明元数据：IID 为EDIT_PLUGIN_INTERFACE_IID
    Q_INTERFACES(EditorPluginInterface); //此类正在尝试实现的是EditorPluginInterface接口
public:
    QString name(); //返回插件操作的名称
    void edit(const cv::Mat &input, cv::Mat &output);
};


#endif // ROTATEPLUGIN_H
