
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QToolBar>
#include <QAction>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QStatusBar>
#include <QLabel>
#include <QGraphicsPixmapItem>
#include <QtCore5Compat>
#include <QMap>
#include "editor_plugin_interface.h" //自定义插件



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow

{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void initUI();
    void createActions();

    void loadPlugins(); //加载某个目录下所有插件


    /////////菜单栏功能
    void showImage(QString path);



    //热键功能
    void setupShortcuts();

private slots:
    ////////////菜单栏各功能
    void openImage(); //显示图像
    void saveAs(); //保存图像
    void zoomIn(); //放大图像
    void zoomOut(); //缩小图像
    void prevImage(); //显示上一张
    void nextImage(); //显示下一张
    void blurImage(); //模糊图像

    //opencv相关操作插件
    void pluginPerform(); //自定义插件公共插槽，连接到已加载插件创建的所有操作


private:
    Ui::MainWindow *ui;

    QMenu *fileMenu;
    QMenu *viewMenu;
    QMenu *editMenu;

    QToolBar *fileToolBar;
    QToolBar *viewToolBar;
    QToolBar *editToolBar;

    QGraphicsScene *imageScene; //仅管理图形项目
    QGraphicsView *imageView; //加上它，才能显示

    QStatusBar *mainStatusBar;
    QLabel *mainStatusLabel;

    //////////////文件栏动作
    QAction *openAction;

    //////////////视图栏动作
    QAction *saveAsAction;
    QAction *exitAction;
    QAction *zoomInAction;
    QAction *zoomOutAction;
    QAction *prevAction;
    QAction *nextAction;

    ///////////////编辑栏动作
    //opencv模糊
    QAction *blurAction;

    QString currentImagePath; //保存当前图像的路径
    QGraphicsPixmapItem *currentImage; //保存图像实例

    //Key为插件名称，Value为插件实例的指针
    QMap<QString, EditorPluginInterface*> editPlugins; //注册所有已加载的插件

};

#endif // MAINWINDOW_H
