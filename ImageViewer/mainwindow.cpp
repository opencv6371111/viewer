
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QKeyEvent>
#include <QDebug>
#include <QPluginLoader>

#include "opencv2/opencv.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , currentImage(nullptr)
{
    ui->setupUi(this);
    initUI();
    loadPlugins(); //加载/plugin下的所有插件
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::initUI()
{
    this->resize(800, 600);
    // setup menubar
    fileMenu = menuBar()->addMenu("&File");
    viewMenu = menuBar()->addMenu("&View");
    editMenu = menuBar()->addMenu("&Edit");

    // setup toolbar
    fileToolBar = addToolBar("File");
    viewToolBar = addToolBar("View");
    editToolBar = addToolBar("Edit");


    // main area for image display
    imageScene = new QGraphicsScene(this);
    imageView = new QGraphicsView(imageScene);
    setCentralWidget(imageView);

    // setup status bar
    mainStatusBar = statusBar();
    mainStatusLabel = new QLabel(mainStatusBar);
    mainStatusBar->addPermanentWidget(mainStatusLabel);
    mainStatusLabel->setText("Image Information will be here!");



    createActions();
}

void MainWindow::createActions()
{
    ////////////创建动作：打开，另存为，退出，放大，缩小，上一张/下一张
    //创建主窗口的子窗口
    // create actions, add them to menus
    openAction = new QAction("&Open", this);
    fileMenu->addAction(openAction);
    saveAsAction = new QAction("&Save as", this);
    fileMenu->addAction(saveAsAction);
    exitAction = new QAction("E&xit", this);
    fileMenu->addAction(exitAction);

    zoomInAction = new QAction("Zoom in", this);
    viewMenu->addAction(zoomInAction);
    zoomOutAction = new QAction("Zoom Out", this);
    viewMenu->addAction(zoomOutAction);
    prevAction = new QAction("&Previous Image", this);
    viewMenu->addAction(prevAction);
    nextAction = new QAction("&Next Image", this);
    viewMenu->addAction(nextAction);


    //////编辑栏
    blurAction = new QAction("Blur", this);
    editMenu->addAction(blurAction);
    editToolBar->addAction(blurAction);



    // add actions to toolbars
    fileToolBar->addAction(openAction);
    viewToolBar->addAction(zoomInAction);
    viewToolBar->addAction(zoomOutAction);
    viewToolBar->addAction(prevAction);
    viewToolBar->addAction(nextAction);

    // connect the signals and slots
    //比如 exitAction 和主窗口的应用实例连接，触发则 quit
    connect(exitAction, SIGNAL(triggered(bool)), QApplication::instance(), SLOT(quit()));
    connect(openAction, SIGNAL(triggered(bool)), this, SLOT(openImage()));
    connect(saveAsAction, SIGNAL(triggered(bool)), this, SLOT(saveAs()));
    connect(zoomInAction, SIGNAL(triggered(bool)), this, SLOT(zoomIn()));
    connect(zoomOutAction, SIGNAL(triggered(bool)), this, SLOT(zoomOut()));
    connect(prevAction, SIGNAL(triggered(bool)), this, SLOT(prevImage()));
    connect(nextAction, SIGNAL(triggered(bool)), this, SLOT(nextImage()));
    connect(blurAction, SIGNAL(triggered(bool)), this, SLOT(blurImage()));

    setupShortcuts();
}

void MainWindow::loadPlugins()
{
    //插件目录
    QDir pluginsDir(QDir::currentPath()+"/../ImageViewer/plugins");
    //插件名称以 .so .dylib .dll 结尾
    QStringList nameFilters;
    nameFilters << "*.so" << "*.dylib" << "*.dll";

    //entryInfoList 用于检索该目录中的文件和子目录信息
    //QDir::NoDotAndDotDot 表示不包括当前目录.和上级目录..在内。
    //QDir::Files 表示只检索文件，不包括子目录。
    //QDir::Name 按照文件名的字典顺序来排序。
    QFileInfoList plugins = pluginsDir.entryInfoList(
        nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);

    foreach(QFileInfo plugin, plugins)
    {
        QPluginLoader pluginLoader(plugin.absoluteFilePath(), this);
        //QPluginLoader实例上调用instance方法。如果已加载目标插件，则将返回指
        //向QObject的指针，否则将返回0。将返回指针转换为插件接口类型。
        EditorPluginInterface *plugin_ptr = dynamic_cast<EditorPluginInterface*>(pluginLoader.instance());
        if(plugin_ptr)
        {
            QAction *action = new QAction(plugin_ptr->name());
            editMenu->addAction(action);
            editToolBar->addAction(action);
            //在editPlugins映射中注册已加载的插件
            editPlugins[plugin_ptr->name()] = plugin_ptr;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(pluginPerform()));
            // pluginLoader.unload();
        }
        else
        {
            qDebug()<<pluginLoader.errorString(); //查看不能加载插件的原因
            qDebug() << "\nbad plugin: " << plugin.absoluteFilePath();
        }
    }
}

void MainWindow::showImage(QString path)
{
    imageScene->clear();
    imageView->resetTransform(); //重置任何变换
    QPixmap image(path);
    currentImage = imageScene->addPixmap(image); //保存image
    imageScene->update(); //更新场景
    imageView->setSceneRect(image.rect()); //范围跟图像大小相同
    //右下角状态栏显示路径、宽x高、文件大小
    QString status = QString("%1, %2x%3, %4 Bytes").arg(path).arg(image.width())
                         .arg(image.height()).arg(QFile(path).size());
    mainStatusLabel->setText(status);
    currentImagePath = path;
}

void MainWindow::setupShortcuts()
{
    //QKeySequence具有带有int参数的非显式构造器，所以可以将Qt::Key值
    //直接添加到列表中，并将它们隐式转换为QKeySequence的实例
    //加号（+）或等于（=）用于放大
    QList<QKeySequence> shortcuts;
    shortcuts << Qt::Key_Plus << Qt::Key_Equal;
    zoomInAction->setShortcuts(shortcuts);

    //QList<QKeySequence>对象填充后，再清空，再填入下一组快捷键
    //减号（-）或下划线（_）用于缩小
    shortcuts.clear();
    shortcuts << Qt::Key_Minus << Qt::Key_Underscore;
    zoomOutAction->setShortcuts(shortcuts);

    //向上或向左查看上一张图像
    shortcuts.clear();
    shortcuts << Qt::Key_Up << Qt::Key_Left;
    prevAction->setShortcuts(shortcuts);

    //向下或向右查看下一张图像
    shortcuts.clear();
    shortcuts << Qt::Key_Down << Qt::Key_Right;
    nextAction->setShortcuts(shortcuts);
}

//放大
void MainWindow::zoomIn()
{
    imageView->scale(1.2, 1.2);
}

void MainWindow::zoomOut()
{
    imageView->scale(0.8, 0.8);
}

void MainWindow::prevImage()
{
    QFileInfo current(currentImagePath);
    QDir dir = current.absoluteDir(); //当前图像所在目录
    QStringList nameFilters; //名称过滤器
    nameFilters << "*.png" << "*.bmp" << "*.jpg";

    //entryList 获取指定目录下的文件和子目录列表
    QStringList fileNames = dir.entryList(nameFilters, QDir::Files, QDir::Name);
    //QRegularExpression对象，它会匹配与current.fileName()完全相同的字符串
    QRegularExpression regex(current.fileName());
    //这里就是指和当前显示图像在当前目录的索引
    int idx = fileNames.indexOf(regex);
    if(idx > 0) {
        showImage(dir.absoluteFilePath(fileNames.at(idx - 1)));
    } else {
        QMessageBox::information(this, "Information", "Current image is the first one.");
    }
}

void MainWindow::nextImage()
{
    QFileInfo current(currentImagePath);
    QDir dir = current.absoluteDir(); //当前图像所在目录
    QStringList nameFilters; //名称过滤器
    nameFilters << "*.png" << "*.bmp" << "*.jpg";

    //entryList 获取指定目录下的文件和子目录列表
    QStringList fileNames = dir.entryList(nameFilters, QDir::Files, QDir::Name);
    //QRegularExpression对象，它会匹配与current.fileName()完全相同的字符串
    QRegularExpression regex(current.fileName());
    //这里就是指和当前显示图像在当前目录的索引
    int idx = fileNames.indexOf(regex);
    if(idx < (fileNames.size()-1) ) {
        showImage(dir.absoluteFilePath(fileNames.at(idx + 1)));
    } else {
        QMessageBox::information(this, "Information", "Current image is the last one.");
    }
}

void MainWindow::blurImage()
{

    if (currentImage == nullptr)
    {
        //弹出消息框
        QMessageBox::information(this, "Information", "No image to edit.");
        return;
    }

    //使用OpenCV的图像矩阵QImage，所以转换
    QPixmap pixmap = currentImage->pixmap();
    QImage image = pixmap.toImage();
    //转换为8位深度、三通道的格式
    image = image.convertToFormat(QImage::Format_RGB888);
    //QImage转换为Mat
    cv::Mat mat = cv::Mat(
        image.height(),
        image.width(),
        CV_8UC3,
        image.bits(),
        image.bytesPerLine());

    //模糊处理
    cv::Mat tmp;
    //第3个参数: 卷积的核大小
    cv::blur(mat, tmp, cv::Size(8, 8)); //输出图像tmp
    mat = tmp;

    //Mat转换回QPixmap格式
    QImage image_blurred(
        mat.data,
        mat.cols,
        mat.rows,
        mat.step,
        QImage::Format_RGB888);
    pixmap = QPixmap::fromImage(image_blurred);

    imageScene->clear();
    imageView->resetTransform();
    currentImage = imageScene->addPixmap(pixmap);
    imageScene->update();
    imageView->setSceneRect(pixmap.rect());
    QString status = QString("(editted image), %1x%2")
                         .arg(pixmap.width()).arg(pixmap.height());
    mainStatusLabel->setText(status);
}

//查找插件对应操作，然后执行操作
void MainWindow::pluginPerform()
{
    if (currentImage == nullptr)
    {
        QMessageBox::information(this, "Information", "No image to edit.");
        return;
    }

    //sender()发送信号并调用插槽。sender()函数返回一个指向QObject实例的指针。
    //object_cast将返回的指针安全地强制转换为QAction的指针。
    //总之就是知道了触发了哪个动作。
    QAction *active_action = qobject_cast<QAction*>(sender());
    //获得动作的文本（这里的动作文本是插件的名称）
    EditorPluginInterface *plugin_ptr = editPlugins[active_action->text()];
    if(!plugin_ptr)
    {
        QMessageBox::information(this, "Information", "No plugin is found.");
        return;
    }

    QPixmap pixmap = currentImage->pixmap();
    QImage image = pixmap.toImage();
    image = image.convertToFormat(QImage::Format_RGB888); //Format_RGB888是Qt的三通道类型
    cv::Mat mat = cv::Mat(
        image.height(),
        image.width(),
        CV_8UC3,
        image.bits(),
        image.bytesPerLine());

    plugin_ptr->edit(mat, mat);

    QImage image_edited(
        mat.data,
        mat.cols,
        mat.rows,
        mat.step,
        QImage::Format_RGB888);
    pixmap = QPixmap::fromImage(image_edited);
    imageScene->clear();
    imageView->resetTransform();
    currentImage = imageScene->addPixmap(pixmap);
    imageScene->update();
    imageView->setSceneRect(pixmap.rect());
    QString status = QString("(editted image), %1x%2")
                         .arg(pixmap.width()).arg(pixmap.height());
    mainStatusLabel->setText(status);
}

void MainWindow::openImage()
{
//    qDebug()<<"slot openImage is called.";

    //////////打开子窗体：从本地选择一个图像文件
    QFileDialog dialog(this);
    dialog.setWindowTitle("Open Image");
    //QFileDialog::ExistingFile 模式只能选择一个现有文件
    dialog.setFileMode(QFileDialog::ExistingFile);
    //名称过滤器
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg)"));
    QStringList filePaths;
    //在对话框上点击“打开”按钮，dialog.exec() 返回一个非零值
    if (dialog.exec()) {
        //获取被选中的文件路径
        filePaths = dialog.selectedFiles();
        showImage(filePaths.at(0)); //显示图像
    }
}

void MainWindow::saveAs()
{
    if (currentImage == nullptr) {
        //告知用户无任何可保存的内存
        QMessageBox::information(this, "Information", "Nothing to save.");
        return;
    }
    QFileDialog dialog(this);
    dialog.setWindowTitle("Save Image As ...");
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg)"));
    QStringList fileNames;
    if (dialog.exec()) { //打开对话框，直到用户触发相关“关闭”信号

        //如果对话框文件名栏提供文件名
        fileNames = dialog.selectedFiles();
        //正则匹配扩展名结尾，.+表示任意多个字符，\\.代表 "." 本身（qt字符串正则中就是双反斜杠）
        //用绝对路径检查最后文件名是否符合支持的扩展名
        if(QRegExp(".+\\.(png|bmp|jpg)").exactMatch(fileNames.at(0))) {
            currentImage->pixmap().save(fileNames.at(0));
        } else {
            QMessageBox::information(this, "Information", "Save error: bad format or filename.");
        }
    }
}














